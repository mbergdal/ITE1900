package com.headcrest.chapter18;

public class Coffee {
    public static void main(String[] args) {
        System.out.printf(
                "It takes %s sips to drink a cup",
                //drinkCoffee(new Cup(new Random().nextInt(99)))
                drinkCoffee(new Cup(4))
        );
    }

    private static int drinkCoffee(Cup cup) {
        int sipsTaken = 0;
        if(!cup.isEmpty()){
            cup.takeOneSip();
            sipsTaken++;
            sipsTaken += drinkCoffee(cup);
        }

        return sipsTaken;
    }
}

class Cup {
    int numberOfSipsLeft;
    public Cup(int i) {
        numberOfSipsLeft = i;
    }

    public boolean isEmpty(){
        return numberOfSipsLeft == 0;
    }

    public void takeOneSip(){
        numberOfSipsLeft--;
    }
}
