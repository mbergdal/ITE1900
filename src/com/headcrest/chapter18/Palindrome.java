package com.headcrest.chapter18;

public class Palindrome {

    public static void main(String[] args) {
        System.out.println("Is the word noon a palindrome? " + isPalindrome("noon"));
        System.out.println("Is the word moon a palindrome? " + isPalindrome("moon"));
    }

    public static boolean isPalindrome(String text) {
        if (text.length() <= 1)
            return true;
        if (text.charAt(0) != text.charAt(text.length()-1))
            return false;
        return isPalindrome(text.substring(1, text.length()-1));
    }

    public static boolean isPalindromeEfficient(String text){
        return isPalindromeEfficient(text, 0, text.length()-1);
    }

    private static boolean isPalindromeEfficient(String text, int low, int high) {
        if (high <= low)
            return true;
        if (text.charAt(low) != text.charAt(high))
            return false;
        return isPalindromeEfficient(text, low+1, high-1);
    }
}
