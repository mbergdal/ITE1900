package com.headcrest.chapter18;

public class PrintNTimes {
    public static void main(String[] args) {
        printNTimesItr("Hello", 5);
    }

    private static void printNTimes(String message, int times) {
        if (times > 0){
            System.out.println(message);
            printNTimes(message, times - 1);
        }
    }

    private static void printNTimesItr(String message, int times) {
        for (int i = 0; i < times; i++) {
            System.out.println(message);
        }
    }
}
