package com.headcrest.chapter18;

import java.util.Scanner;
//http://www.dynamicdrive.com/dynamicindex12/towerhanoi.htm

public class TowerOfHanoi {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter number of disks: ");
        int numberOfDisks = input.nextInt();

        System.out.println("The moves are:");
        moveDisks(numberOfDisks, 'A', 'C', 'B');
    }

    private static void moveDisks(int n, char fromTower, char toTower, char helperTower) {
        if (n == 1){
            System.out.println("Move disk " + n + " from " + fromTower + " to " + toTower);
        }
        else{
            moveDisks(n-1, fromTower, helperTower, toTower);
            System.out.println("Move disk " + n + " from " + fromTower + " to " + toTower);
            moveDisks(n-1, helperTower, toTower, fromTower);
        }
    }
}
