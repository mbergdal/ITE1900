package com.headcrest.chapter18;

public class Fibonacci {
    public static void main(String[] args) {

        System.out.println("The eleventh fibonacci number is: " + fibonacci(4));
    }

    public static long fibonacci(int n) {
        System.out.println("n: " + n);
        if (n == 0)
            return 0;
        if (n == 1)
            return 1;
        return fibonacci(n-2) + fibonacci(n-1);

    }
}
