package com.headcrest.chapter18;

public class Factorial {

    public static void main(String[] args) {
        System.out.println("The factorial of 6 is: " + factorial(6));
    }

    public static int factorial(int i) {
        if (i == 0)
            return 1;
       return i * factorial(i-1);
    }
}
