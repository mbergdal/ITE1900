package com.headcrest.chapter1516;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class ShowCircleCentered extends Application {
  @Override // Override the start method in the Application class
  public void start(Stage primaryStage) {    

    BorderPane borderPane = new BorderPane();
    Pane pane = new Pane();

    Circle circle = new Circle();

    circle.getCenterX();


    circle.centerXProperty().bind(borderPane.widthProperty().divide(2));
    circle.centerYProperty().bind(borderPane.heightProperty().divide(2));
    circle.setRadius(50);
    circle.setStroke(Color.BLACK); 
    circle.setFill(Color.GREENYELLOW);
    pane.getChildren().add(circle);

    Slider slider = new Slider(30,100, 50);
    circle.radiusProperty().bind(slider.valueProperty());
    circle.getCenterX();

    //Adding Listener to Observable.
    slider.valueProperty().addListener( x -> System.out.println("Value is: " + slider.getValue()));

    TextField textField = new TextField();
    Bindings.bindBidirectional(textField.textProperty(), slider.valueProperty(), new NumberStringConverter());

    borderPane.setBottom(slider);
    borderPane.setCenter(pane);
    borderPane.setTop(textField);

    Scene scene = new Scene(borderPane, 300, 300);
    primaryStage.setTitle("ShowCircleCentered"); // Set the stage title
    primaryStage.setScene(scene); // Place the scene in the stage
    primaryStage.show(); // Display the stage
  }
  
  /**
   * The main method is only needed for the IDE with limited
   * JavaFX support. Not needed for running from the command line.
   */
  public static void main(String[] args) {
    launch(args);
  }
}
