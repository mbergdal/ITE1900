package com.headcrest.chapter141516;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class MyFirstJavaFXApp extends Application {

    int myInt = 4;

    private void myMethod(){
        myInt = 3;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Button button = new Button("Touch me!");
        Button button2 = new Button("Help!");
        Circle c = new Circle();
        c.setRadius(55);
        //c.setFill(Color.BLUE);
        //c.setStroke(Color.RED);

        c.setStyle("-chapter1516-fill:blue; -chapter1516-stroke:red");

        int i = 0;
        button.setOnAction(event -> {

            System.out.println(event.getSource().getClass().getCanonicalName() + c.getRadius());
            System.out.println("Hei");
        });
        button.setOnMouseEntered((MouseEvent event) -> {
            System.out.println("Mouse entered");
        });

        GridPane pane = new GridPane();
        pane.add(button, 0, 0);
        pane.add(button2,0,1);
        pane.add(c, 1, 0);
        pane.setGridLinesVisible(true);

        Scene scene = new Scene(pane, 200, 200);

        scene.setOnKeyPressed(event -> System.out.println(event.getText()));

        primaryStage.setTitle("My app");
        primaryStage.setScene(scene);

        primaryStage.show();

    }
}
