package com.headcrest.chapter141516;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class FourImages extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane pane = new GridPane();
        ImageView image1 = new ImageView("/com/headcrest/chapter141516/images/image/flag0.gif");
        ImageView image2 = new ImageView("/com/headcrest/chapter141516/images/image/flag1.gif");
        ImageView image3 = new ImageView("/com/headcrest/chapter141516/images/image/flag2.gif");
        ImageView image4 = new ImageView("/com/headcrest/chapter141516/images/image/flag3.gif");

        pane.add(image1, 0,0);
        pane.add(image2, 0,1);
        pane.add(image3, 1,0);
        pane.add(image4, 1,1);

        Scene scene = new Scene(pane);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
