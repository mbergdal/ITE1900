package com.headcrest.chapter141516.events;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class EventTest extends Application {
    private TextField textField = new TextField();
    int n = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button b = new Button("Click me!");


        EventHandler<ActionEvent> myButtonEventHandler = new MyButtonEventHandler();
        b.setOnAction(myButtonEventHandler);

        //b.setOnAction(new MyButtonEventHandler());

//        b.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent event) {
//                n += 1;
//                textField.setText("Button Clicked" + n);
//            }
//        });

//        b.setOnAction( event -> {
//            n += 1;
//            textField.setText("Button Clicked " + n);
//        });

        FlowPane pane = new FlowPane(Orientation.VERTICAL, b, textField);
        Scene scene = new Scene(pane, 200, 200);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private class MyButtonEventHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            n += 1;
            textField.setText("Button Clicked " + n);
        }
    }
}
