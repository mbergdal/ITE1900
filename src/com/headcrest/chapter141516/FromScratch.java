package com.headcrest.chapter1516;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

public class FromScratch extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button button = new Button("Hello World");

        //button.setStyle("-chapter1516-background-color: aqua");
        Alert a = new Alert(Alert.AlertType.INFORMATION, "Hello", ButtonType.CLOSE);
        button.setOnAction(x -> primaryStage.close());
        Scene scene = new Scene(button);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
