package com.headcrest.chapter141516;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class MyJavaFXApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button button = new Button("Hello world!");

        button.setOnAction(x -> {
            System.out.println("Button clicked!");
            //Platform.exit();
        });
        button.setOnMouseEntered( x-> System.out.println("Mouse entered"));

        
        Scene scene = new Scene(button, 200, 200);

        scene.setOnKeyPressed( x -> System.out.println(x.getText()));

        primaryStage.setScene(scene);

        primaryStage.show();

    }
}
