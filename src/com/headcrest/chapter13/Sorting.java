package com.headcrest.chapter13;

import java.util.Arrays;

public class Sorting {
    public static void main(String[] args) {
        String[] numbers = {"6", "7", "3", "2", "9", "5"};
        Arrays.sort(numbers); //How does this work? (compareTo)

        for (String s: numbers) {
            System.out.println(s);
        }

        // This also works. Why?
        int[] ints = {3,1,2};
        Arrays.sort(ints);

        for (int i : ints){
            System.out.println(i);
        }
    }
}
