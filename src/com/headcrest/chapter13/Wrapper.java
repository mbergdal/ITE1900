package com.headcrest.chapter13;

import java.util.ArrayList;

public class Wrapper {
    public static void main(String[] args) {

        Integer one = 1; //boxing
        int o = one; //unboxing

        ArrayList<Number> integers = new ArrayList<>();
        integers.add(1);
        integers.add(new Integer(3));

        int sum = 0;
        for (Number i : integers)
            sum += i.intValue();

        System.out.println(sum);
    }
}
