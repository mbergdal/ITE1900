package com.headcrest.chapter13.edible;

public interface Edible {
  /** Describe how to eat */
  String howToEat();
}
