package com.headcrest.chapter13.edible;

import java.util.ArrayList;

//Example from p.508
public class TestEdible {

  public static void main(String[] args) {

      ArrayList<Edible> edibleThings = new ArrayList<>();

      edibleThings.add(new Orange());
      edibleThings.add(new Apple());
      //edibleThings.add(new Tiger());

      for (Edible x : edibleThings) {
          System.out.println(x.howToEat());
      }

    /*Object[] objects = {new Tiger(), new Chicken(), new Apple()};

    for (int i = 0; i < objects.length; i++) {

        if (objects[i] instanceof Edible)
            System.out.println(((Edible)objects[i]).howToEat());

        if (objects[i] instanceof Animal) {
            System.out.println(((Animal)objects[i]).sound());
        }
    }*/
  }
}

