package com.headcrest.chapter13.edible;

public abstract class Animal {
  public abstract String sound();
}
