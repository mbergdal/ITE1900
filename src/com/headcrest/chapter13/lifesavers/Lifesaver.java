package com.headcrest.chapter13.lifesavers;

public interface Lifesaver {
    void saveLife();
}
