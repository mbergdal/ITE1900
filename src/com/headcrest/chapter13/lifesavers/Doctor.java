package com.headcrest.chapter13.lifesavers;

public class Doctor extends Person implements Lifesaver {

    public Doctor(String name) {
        super(name);
    }

    @Override
    public void saveLife() {
        System.out.println("Doctor saved a life");
    }
}
