package com.headcrest.chapter13.lifesavers;

public class Firefighter extends Person implements Lifesaver {

    public Firefighter(String name) {
        super(name);
    }

    @Override
    public void saveLife() {
        System.out.println("Firefighter saved a life");
    }
}
