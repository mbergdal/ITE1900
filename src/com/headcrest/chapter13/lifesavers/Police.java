package com.headcrest.chapter13.lifesavers;

public class Police extends Person implements Lifesaver {

    public Police(String name) {
        super(name);
    }

    @Override
    public void saveLife() {
        System.out.println("PoliceOfficer saved a life");
    }
}
