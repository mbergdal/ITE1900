package com.headcrest.chapter13.lifesavers;

import java.util.ArrayList;

public class TestLifesavers {

    public static void main(String[] args) {
        ArrayList<Lifesaver> persons = new ArrayList<>();

        //Put different persons in list
        persons.add(new Police("Pelle"));
        persons.add(new Firefighter("Tore"));
        persons.add(new Doctor("Siri"));
        //persons.add(new Student("Thor"));

        for (Lifesaver p: persons) {
            p.saveLife();
            Object o = null;
        }
    }
}
