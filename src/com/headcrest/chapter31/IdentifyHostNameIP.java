package com.headcrest.chapter31;

import java.net.*;
import java.util.Scanner;

public class IdentifyHostNameIP {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Give some hostnames: ");
        String line = in.nextLine();
        String[] strings = line.split(" ");

        for (int i = 0; i < strings.length; i++) {
            try {
                InetAddress address = InetAddress.getByName(strings[i]);
                System.out.print("Host name: " + address.getHostName() + " ");
                System.out.println("IP address: " + address.getHostAddress());
            } catch (UnknownHostException ex) {
                System.err.println("Unknown host or IP address " + strings[i]);
            }
        }
    }
}
