package com.headcrest.chapter31;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ChatServer {

    private static ArrayList<Socket> clients = new ArrayList<>();

    public static void main(String[] args) {

        try {
            ServerSocket serverSocket = new ServerSocket(8002);
            System.out.println("Server started. Ready for connection");

            while(true){
                Socket socket = serverSocket.accept();
                clients.add(socket);
                System.out.println("Client nbr " + clients.size() + " connected. Ip: " +
                        socket.getInetAddress().getHostAddress() + " host name; " + socket.getInetAddress().getHostName());

                new Thread(new HandleAClient(socket)).start();
                if(clients.size() == 1){
                    System.out.println("Waiting for another client");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class HandleAClient implements Runnable {
        private Socket mySocket;

        public HandleAClient(Socket socket) {
            mySocket = socket;
        }

        @Override
        public void run() {
            try {
                DataInputStream clientInputStream = new DataInputStream(mySocket.getInputStream());

                while(true) {
                    if(clientInputStream.available() != 0) {
                        String message = clientInputStream.readUTF();

                        System.out.println(message);
                        for (Socket s : clients) {
                            if(s != mySocket) {
                                new DataOutputStream(s.getOutputStream()).writeUTF(message);
                            }
                        }
                    }else{
                        Thread.sleep(500);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
