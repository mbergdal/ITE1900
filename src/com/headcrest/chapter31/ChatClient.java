package com.headcrest.chapter31;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ChatClient extends Application {

    DataOutputStream toServer = null;
    DataInputStream fromServer = null;
    TextField messageField = null;
    TextField nickField = null;
    TextArea textArea = null;
    Stage primaryStage = null;

    String server = "localhost";
    private String nick;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        setUpGUI();
        setUpKeyHandlers();
        connectToServer();
        handleMessageFromServer();
    }

    private void handleMessageFromServer() {
        new Thread(() -> {
            while(true){
                try {
                    if(fromServer.available() != 0) {
                        String messageFromServer = fromServer.readUTF();
                        Platform.runLater(() -> textArea.appendText(messageFromServer + '\n'));
                    }else{
                        Thread.sleep(500);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void setUpKeyHandlers() {
        messageField.setOnAction(e -> {
            try {
                if (nick != null && !nick.isEmpty()) {
                    String message = messageField.getText().trim();
                    toServer.writeUTF(nick + ": " + message);
                    toServer.flush();
                    messageField.clear();
                    nickField.setStyle("-chapter1516-border-color: white");
                }
                else{
                    nickField.setStyle("-chapter1516-border-color: red");
                }
            }catch(IOException ex){
                System.err.println(ex);
            }
        });

        nickField.setOnKeyPressed(e -> {
            nick = nickField.getText().trim();
            if(!nick.isEmpty())
                primaryStage.setTitle(nick);
        });
    }

    private void setUpGUI() {
        BorderPane paneForTextField = new BorderPane();
        paneForTextField.setPadding(new Insets(5, 5, 5, 5));
        paneForTextField.setStyle("-chapter1516-border-color: green");

        messageField = new TextField();
        messageField.setPromptText("Message");
        nickField = new TextField();
        nickField.setPromptText("Nick");
        messageField.setAlignment(Pos.BOTTOM_RIGHT);
        nickField.setAlignment(Pos.BOTTOM_CENTER);
        paneForTextField.setBottom(messageField);
        paneForTextField.setTop(nickField);
        BorderPane mainPane = new BorderPane();
        textArea = new TextArea();
        textArea.setEditable(false);
        mainPane.setCenter(new ScrollPane(textArea));
        mainPane.setTop(paneForTextField);

        Scene scene = new Scene(mainPane, 450, 200);
        primaryStage.setTitle("Client");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void connectToServer() {
        try {
            Socket socket = new Socket(server, 8002);
            System.out.println("Client host name: " + socket.getInetAddress().getCanonicalHostName());
            System.out.println("Client ip: " + socket.getInetAddress().getHostAddress());
            System.out.println("Local port: " + socket.getLocalPort());

            fromServer = new DataInputStream(socket.getInputStream());
            toServer = new DataOutputStream(socket.getOutputStream());

        } catch (IOException ex) {
            textArea.appendText(ex.toString() + '\n');
        }
    }
}
