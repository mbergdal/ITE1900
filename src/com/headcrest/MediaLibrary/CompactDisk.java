package com.headcrest.MediaLibrary;

public class CompactDisk extends MediaItem {

    public CompactDisk(String title) {
        super(title);
    }

    @Override
    public String getMetadata() {
        return "Format:CD Tracs:12";
    }
}
