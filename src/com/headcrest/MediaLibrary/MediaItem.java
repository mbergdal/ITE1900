package com.headcrest.MediaLibrary;

public abstract class MediaItem  {
    private final String title;

    public MediaItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString(){
        return getTitle();
    }

    public String getMetadata() {
        return "No metadata";
    }
}
