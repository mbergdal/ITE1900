package com.headcrest.MediaLibrary;


public class Movie extends MediaItem {

    public Movie(String title) {
        super(title);
    }

    @Override
    public String getMetadata() {
        return "Format:DVD - Label:Disney";
    }
}
