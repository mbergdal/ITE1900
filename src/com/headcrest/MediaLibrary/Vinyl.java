package com.headcrest.MediaLibrary;

public class Vinyl extends MediaItem{

    public Vinyl(String title) {
        super(title);
    }

    @Override
    public String getMetadata() {
        return "Format:Vinyl - Label:Columbia";
    }
}
