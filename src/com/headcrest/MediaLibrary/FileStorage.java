package com.headcrest.MediaLibrary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FileStorage {

    private final File file;

    public FileStorage(String fileName) {
        this.file = new File(fileName);
    }

    public void save(MediaItem item) {
        try (PrintWriter writer = new PrintWriter(file)){
            writer.print(item.getTitle());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(List<MediaItem> items) {
        try (PrintWriter writer = new PrintWriter(file)){
            for (MediaItem item : items)
                writer.print(item.getTitle() + item.getMetadata() + "\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
