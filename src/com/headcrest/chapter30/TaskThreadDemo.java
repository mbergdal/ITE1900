package com.headcrest.chapter30;

public class TaskThreadDemo {
    public static void main(String[] args) {
        // Create tasks
        Runnable printA = new PrintChar('a', 100);
        Runnable printB = new PrintChar('b', 100);
        Runnable print100 = new PrintNum(100);

        // Create chapter30
        Thread thread1 = new Thread(printA);
        //thread1.setPriority(Thread.MIN_PRIORITY);
        Thread thread2 = new Thread(printB);
        //thread2.setPriority(Thread.NORM_PRIORITY);
        Thread thread3 = new Thread(print100);
        //thread3.setPriority(Thread.MAX_PRIORITY);

        // Start chapter30
        thread3.start();
        //thread3.run(); //This executes the run-method on the same thread as the main-method
        thread1.start();
        thread2.start();

        //Thread.State state = thread1.getState();
    }
}

// The task for printing a specified character in specified times
class PrintChar implements Runnable {
    private char charToPrint; // The character to print
    private int times; // The times to repeat

    /** Construct a task with specified character and number of
     *  times to print the character
     */
    public PrintChar(char c, int t) {
        charToPrint = c;
        times = t;
    }

    /** Override the run() method to tell the system
     *  what the task to perform
     */
    public void run() {
        for (int i = 0; i < times; i++) {
            System.out.print(charToPrint);
            //Thread.yield();
        }
    }
}

// The task class for printing number from 1 to n for a given n
class PrintNum implements Runnable {
    private int lastNum;

    /** Construct a task for printing 1, 2, ... i */
    public PrintNum(int n) {
        lastNum = n;
    }

    /** Tell the thread how to run */
    public void run() {
        //Thread thread4 = new Thread(new PrintChar('c', 40));
        //thread4.start();
        for (int i = 1; i <= lastNum; i++) {
            System.out.print(" " + i);
//            if (i == 50)
//                try {
//                    thread4.join(); // The numbers >50 are printed after thread4 is finished
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
            //Thread.yield();

//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

        }
    }
}
