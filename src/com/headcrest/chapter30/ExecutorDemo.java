package com.headcrest.chapter30;

import java.util.concurrent.*;

public class ExecutorDemo {
  public static void main(String[] args) {
    // Create a fixed thread pool with maximum three chapter30
    ExecutorService executor = Executors.newFixedThreadPool(3); //What happens if we set this to 1?

    // Submit runnable tasks to the executor
    executor.execute(new PrintChar('a', 100));
    executor.execute(new PrintChar('b', 100));
    executor.execute(new PrintNum(100));
    executor.execute(new PrintChar('f', 100));

    // Shut down the executor
    executor.shutdown();
  }
}
