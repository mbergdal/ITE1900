package com.headcrest.chapter30.evilThread;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Label label;
    @FXML
    private ProgressIndicator progress;
    private Thread theEvilThread;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        startProgressAnimation();
        initializeTheOtherThread();
    }

    private void initializeTheOtherThread() {
        theEvilThread = new Thread(() -> {

            Platform.runLater(() -> {
                label.setText("The evil thread started");
            });

            while(true){
                System.out.println("Hello from the evil thread");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void startProgressAnimation() {
        Timeline progressAnimation = new Timeline(new KeyFrame(Duration.millis(500), e -> updateProgress()));
        progressAnimation.setCycleCount(Timeline.INDEFINITE);
        progressAnimation.play();
    }

    private void updateProgress() {
        double currentProgress = progress.getProgress();
        if(currentProgress >= 1){
            progress.setProgress(0);
        }else{
            progress.setProgress(currentProgress + 0.1);
        }
    }

    public void onStartButtonClicked(ActionEvent actionEvent) {
        if(!theEvilThread.isAlive()) {
            theEvilThread.start();
        }
    }
}
