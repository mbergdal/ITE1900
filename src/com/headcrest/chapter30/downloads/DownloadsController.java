package com.headcrest.chapter30.downloads;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadsController implements Initializable {

    @FXML
    private ProgressBar progressFile1, progressFile2, progressFile3, progressFile4, progressFile5;
    @FXML
    private Label label1, label2, label3, label4, label5;
    @FXML
    private Slider slider;
    private Label[] labels;
    private ProgressBar[] progressFiles;
    private ExecutorService executor;

    private String[] urls = new String[]{
            "http://studylogic.net/wp-content/uploads/2013/01/burger.jpg",
            "http://smokeybones.com/wp-content/uploads/2015/11/smokehouse-burger.jpg",
            "https://i.ytimg.com/vi/KY4IzMcjX3Y/maxresdefault.jpg",
            "http://www.altunsa.co/images/phocagallery/thumbs/phoca_thumb_l_1r%20fast%20food%20pictures%20food%20photos%20%20images%20%20fotos.jpg",
            "http://i.kinja-img.com/gawker-media/image/upload/asep1x4xtenjlpicxbnb.png"
    };

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        labels = new Label[]{label1, label2, label3, label4, label5};
        progressFiles = new ProgressBar[]{progressFile1, progressFile2, progressFile3, progressFile4, progressFile5};
    }

    public void onStartButtonClicked(ActionEvent actionEvent) throws InterruptedException, IOException {
        resetProgress();
        downloadWithoutThreads();
        //downloadWithThreads();
        //downloadWithThreadPool();
    }

    private void downloadWithoutThreads() {
        getFileSizeAndDownload(urls[0], progressFile1, label1);
        getFileSizeAndDownload(urls[1], progressFile2, label2);
        getFileSizeAndDownload(urls[2], progressFile3, label3);
        getFileSizeAndDownload(urls[3], progressFile4, label4);
        getFileSizeAndDownload(urls[4], progressFile5, label5);
    }

    private void downloadWithThreads() {
        new Thread(() -> {
               getFileSizeAndDownload(urls[0], progressFile1, label1);
        }).start();

        new Thread(() -> {
            getFileSizeAndDownload(urls[1], progressFile2, label2);
        }).start();

        new Thread(() -> {
            getFileSizeAndDownload(urls[2], progressFile3, label3);
        }).start();

        new Thread(() -> {
            getFileSizeAndDownload(urls[3], progressFile4, label4);
        }).start();

        new Thread(() -> {
            getFileSizeAndDownload(urls[4], progressFile5, label5);
        }).start();
    }

    private void downloadWithThreadPool() {
        int numberOfThreads = (int)slider.getValue();
        executor = Executors.newFixedThreadPool(numberOfThreads);
        for (int i = 0; i <= 4; i++) {
            final int finalI = i;
            executor.execute(() -> {
                getFileSizeAndDownload(urls[finalI], progressFiles[finalI], labels[finalI]);
            });
        }
    }

    private void getFileSizeAndDownload(String urlString, ProgressBar progressBar, Label label) {
        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.connect();
            int size = conn.getContentLength();
            downloadFile(url, progressBar, label, size);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private long downloadFile(URL url, ProgressBar progressFile1, Label label, int size){
        long nbrBytesRead = 0;
        try (BufferedInputStream input = new BufferedInputStream(url.openStream())){
            while (input.read() != -1) {
                nbrBytesRead += 1;
                if(nbrBytesRead % 1024 == 0) {
                    updateProgress(progressFile1, label, size, nbrBytesRead);
                }
            }

            System.out.println("Downloaded: " + nbrBytesRead + " bytes from " + url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return nbrBytesRead;
    }

    private void updateProgress(ProgressBar progressFile1, Label label, int size, long nbrBytesRead) {
        Platform.runLater(() -> {
            progressFile1.setProgress((double) nbrBytesRead / size);
            label.setText(Long.toString(nbrBytesRead) + " bytes");
        });
    }

    private void resetProgress() {
        progressFile1.setProgress(0);
        progressFile2.setProgress(0);
        progressFile3.setProgress(0);
        progressFile4.setProgress(0);
        progressFile5.setProgress(0);
    }

    public void onStopButtonClicked(ActionEvent actionEvent) {
        if(executor != null) {
            List<Runnable> runnables = executor.shutdownNow();
            for (Runnable runnable : runnables) {
                System.out.println(runnable);
            }
        }
    }
}
