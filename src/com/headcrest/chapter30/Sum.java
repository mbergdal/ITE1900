package com.headcrest.chapter30;
import java.util.concurrent.*;

//Ex 30.4
public class Sum {

    private Integer sum = new Integer(0);

    public Sum() {
        ExecutorService executor = Executors.newFixedThreadPool(1000);

        for (int i = 0; i < 1000; i++) {
            executor.execute(new SumTask());
        }

        executor.shutdown();

        while(!executor.isTerminated()) {
        }
    }

    public static void main(String[] args) {
        Sum test = new Sum();
        System.out.println("What is sum ? " + test.sum);
    }

    class SumTask implements Runnable {
        String lock = "";

        public void run() {
           // sum++;

            //synchronized(lock) {
                int value = sum.intValue() + 1;
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sum = new Integer(value);
            //}
        }
    }
}

