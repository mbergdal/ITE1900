package com.headcrest.chapter11.inheritance;

import com.sun.org.apache.regexp.internal.RE;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayList<GeometricObject> geometricObjects = new ArrayList<>();

        GeometricObject circle = (GeometricObject) new Circle(2.0); // implicit casting  because Circle is-a GeometricObject (upcasting)

        System.out.println(circle.toString());
        geometricObjects.add(circle);


        GeometricObject rectangle = new Rectangle(2, 3);
        geometricObjects.add(rectangle);

        GeometricObject tri = new Triangle(3, 6, 7.4);
        geometricObjects.add(tri);
        System.out.println(tri.toString() + ((Triangle)tri).getArea());

        Object[] objects = geometricObjects.toArray();
        Arrays.sort(objects);

        for (Object o: objects) {
            System.out.println(o.toString());
        }

        //Generic programming
        printWithPrefix("Hello", rectangle);

        for (GeometricObject obj: geometricObjects) {
            System.out.println(obj.getColor());
            System.out.println(obj.getArea()); //<- Why is this problematic?
        }
    }



    //Example of generic programming
    private static void printWithPrefix(String prefix, GeometricObject object) {
        if (object instanceof Rectangle) {
            Rectangle rectangle = (Rectangle)object; //Must do a cast here. (explisit downcasting)
            String stringWithPrefix = rectangle.toString(prefix);
            System.out.println(stringWithPrefix);
        }else
            System.out.println(object.toString());
    }
}
