package com.headcrest.chapter11.inheritance;

public class Rectangle extends GeometricObject {

    private final double heigth;
    private final double length;

    public Rectangle(double heigth, double length) {
        this.heigth = heigth;
        this.length = length;
    }

    @Override
    public double getArea() {
        return heigth * length;
    }

    public String toString(String prefix) {
        return prefix + super.toString();
    }
}
