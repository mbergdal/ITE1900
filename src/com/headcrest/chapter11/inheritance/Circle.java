package com.headcrest.chapter11.inheritance;

public class Circle extends GeometricObject{
    private double radius;
    private double area;

    public Circle(double radius) {
        //super(); //This is not necessary, because of the way inheritance works
        //super("red"); //But this is not
        this.radius = radius;
    }

    @Override
    public double getArea() {
        area = Math.PI * radius * radius;
        return area;
    }

    @Override
    //This method is redundant because of dynamic binding
    public String toString(){
        String superString = super.toString();
        return superString + " I'm a circle";
    }

    //Overload
    public String toString(String prefix){
        return prefix + " " + this.toString();
    }

    @Override
    public boolean equals(Object c){
        if (c instanceof Circle){
            if (this.radius == ((Circle)c).radius){
                return true;
            }
        }

        return false;
    }


}

















