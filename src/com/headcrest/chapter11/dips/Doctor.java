package com.headcrest.chapter11.dips;

import javax.print.Doc;

public class Doctor extends Employee{

    private boolean isPissed;

    public Doctor(String name){
        super(name);
    }

    public void speak(){
        System.out.println("Hello");
    }

    public void speak(String words){
        System.out.println(words);
    }

    public String toString(){
        if (isPissed)
            return super.toString();
        return getName();
    }

    public boolean equals(Object anObject){
        if (anObject instanceof Doctor) {
            Doctor d = (Doctor)anObject;
            if (this.getName() == d.getName())
                return true;
        }

        return false;
    }
}
