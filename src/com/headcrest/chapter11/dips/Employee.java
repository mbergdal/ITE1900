package com.headcrest.chapter11.dips;

public class Employee extends Person{
    private int eployeeNbr;

    public Employee(){
        this("Unnamed");
    }

    public Employee(String name){
       super(name);
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    private double salary;

    public int getEployeeNbr() {
        return eployeeNbr;
    }

    public double getSalary() {
        return salary;
    }

    public String toString(){
        return "Employee";
    }

    public String callObjectToString(){
        return super.toString();
    }

}
