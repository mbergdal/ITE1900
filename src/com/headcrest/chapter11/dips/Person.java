package com.headcrest.chapter11.dips;

public class Person extends Object{

    public Person(){

    }

    public Person(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private String name;
}
