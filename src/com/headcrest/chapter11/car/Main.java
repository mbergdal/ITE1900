package com.headcrest.chapter11.car;

public class Main {

    public static void main(String[] args) {
        Car car = new Car();
        car.drive();

        System.out.println(car.toString());
    }
}
