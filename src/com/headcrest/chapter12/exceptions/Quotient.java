package com.headcrest.chapter12.exceptions;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Quotient {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Enter two integers: ");

            try {
                int int1 = input.nextInt();
                int int2 = input.nextInt();
                double quotient = findQuotient(int1, int2);
                System.out.println(quotient);
            } catch (IgnorantUserException e) {
                System.out.println(e.getMessage());
                //e.printStackTrace();
            } catch (InputMismatchException e){
                input.nextLine();
                System.out.println("Only integers please!");
            }
        }
    }

    private static double findQuotient(int int1, int int2) throws IgnorantUserException{
        if (int2 == 0){
            throw new IgnorantUserException("divisor can not be 0");

        }else
            return (double)int1 / int2;
    }
}