package com.headcrest.chapter12.exceptions;

public class ArrayException {

    public static void main(String[] args) {
        int[] myArray = new int[]{1, 2, 4};
        System.out.println(myArray[3]);
    }
}
