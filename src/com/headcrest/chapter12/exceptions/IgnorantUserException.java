package com.headcrest.chapter12.exceptions;


public class IgnorantUserException extends Exception {

    public IgnorantUserException(String message){
        super(message);
    }
}
