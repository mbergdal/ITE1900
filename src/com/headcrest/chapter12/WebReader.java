package com.headcrest.chapter12;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class WebReader {
    public static void main(String[] args) {
        try {
            URL myUrl = new URL("http://www.google.com/index.html");
            Scanner input = new Scanner(myUrl.openStream());

            while (input.hasNextLine()){
                System.out.println(input.nextLine());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
