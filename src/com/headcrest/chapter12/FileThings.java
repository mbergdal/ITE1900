package com.headcrest.chapter12;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileThings {

    public static void main(String[] args) {
        String fileName = "test.txt";
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(fileName);
            writer.println("Hello World!");
            writer.print(" Mads");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }

        //System.out.println(readFromFile(fileName));
    }

    private static String readFromFile(String fileName) throws Exception{
        String output = "";
        File file = new File(fileName);


        //printPathOfCurrentFile(file);

        //See documentation....
        try (Scanner input = new Scanner(file)){ //Try-with - autoclosable interface...
            while (input.hasNextLine()){
                output += input.nextLine();
            }
        } catch (FileNotFoundException e) {
            throw new Exception("uups", e);
        }

        return output;
    }

    private static void printPathOfCurrentFile(File file){
        final String dir = System.getProperty("user.dir"); //Gets the directory where the java command was launched from
        System.out.println("current dir = " + dir);
        System.out.println("Separator is: " + File.separator);
        System.out.println("Absolute path of file is: " + file.getAbsolutePath());
        System.out.println("Relative path of file is: " + file.getPath());
    }

}
