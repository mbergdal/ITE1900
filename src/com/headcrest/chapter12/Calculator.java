package com.headcrest.chapter12;

import com.headcrest.chapter12.exceptions.IgnorantUserException;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while(true) {
            System.out.print("Enter two integers: ");

            try {
                int first = input.nextInt();
                int second = input.nextInt();
                int quotient = getQuotient(first, second);
                System.out.println(quotient);
            } catch (IgnorantUserException e) {
                //e.printStackTrace();
                System.out.println(e.getMessage());
            } catch (Exception e){
                System.out.println("The number must be an integer");
                input.nextLine();
            } finally {
                System.out.println("Nice job!");
            }
        }
    }

    private static int getQuotient(int first, int second) throws IgnorantUserException{
        if (second == 0){
            throw new IgnorantUserException("Second number can not be 0");
        } else {
            return first / second;
        }
    }
}
