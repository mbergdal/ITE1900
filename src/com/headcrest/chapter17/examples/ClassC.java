package com.headcrest.chapter17.examples;

import java.io.Serializable;


public class ClassC implements Serializable{
    private int a;
    private static int b = 2;
    private ClassA anotherClass; //transient

    public ClassC(){
        a = 1;
        anotherClass = new ClassA();
    }

    @Override
    public String toString(){
        return "a: " + a + " b: " + b + " ClassA: " + ((anotherClass == null) ? "null" : anotherClass.toString());
    }
}
