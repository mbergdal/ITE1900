package com.headcrest.chapter17.examples;

import java.io.*;
import java.util.Scanner;

public class FileInputOutput {

    public static void main(String[] args) throws IOException {
        try(FileOutputStream outputStream = new FileOutputStream("chapter17.dat");){
            outputStream.write(255); // What about writing the number 257?
        }

        try(PrintWriter output = new PrintWriter("text.txt");){
            output.print(199);
        }

        byte[] bytes = "199".getBytes();

        readFromBinaryFile();
        System.out.println();
        readFromTextFile();

    }

    private static void readFromTextFile() throws IOException{
        System.out.print("Text: ");
        try(Scanner input = new Scanner(new File("text.txt"));){
            System.out.print(input.nextInt());
        }
    }

    private static void readFromBinaryFile() throws IOException {
        System.out.print("Binary: ");
        try(FileInputStream inputStream = new FileInputStream("chapter17.dat")){ //Note try-with construct
            int value;
            while((value = inputStream.read()) != -1) {
                System.out.println(value);
            }
        }
    }
}
