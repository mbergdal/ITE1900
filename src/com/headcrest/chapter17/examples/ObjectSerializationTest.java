package com.headcrest.chapter17.examples;

import java.io.*;

public class ObjectSerializationTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ClassC c = new ClassC();
        System.out.println(c);
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("objectC.dat"));){
            output.writeObject(c);
        }

        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream("objectC.dat"));){
            ClassC inputC = (ClassC) input.readObject();
            System.out.println(inputC);
        }
    }




}
