package com.headcrest.chapter17.examples;

import java.io.*;

public class FilterInputOutput {
    public static void main(String[] args) throws IOException {

        try (DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("utf.dat"));) {
            outputStream.writeUTF("ABCDEF"); //Hva med ABCDEÆ?
        }

        try (DataInputStream inputStream = new DataInputStream(new FileInputStream("utf.dat"));) {
            int s = inputStream.readInt();
            System.out.print(s);
        }
    }
}
